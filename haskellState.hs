module Main where


import Text.Read
import Data.Maybe


data Board = Board [[String]]

------

drawBoard :: Board -> IO ()
drawBoard (Board board)= do
  putStrLn $ mconcat ["3 ", board!!0!!0, "#", board!!0!!1, "#", board!!0!!2]
  putStrLn "  #####"
  putStrLn $ mconcat ["2 ", board!!1!!0, "#", board!!1!!1, "#", board!!1!!2]
  putStrLn "  #####"
  putStrLn $ mconcat ["1 ", board!!2!!0, "#", board!!2!!1, "#", board!!2!!2]
  putStrLn "  1 2 3"

------

mapWInd :: (a -> Int -> b) -> [a] -> [b] -- map function that also gives the index
mapWInd function list = zipWith function list [0..] 

------

replace :: Int -> a -> [a] -> [a] -- takes a list and returns a new list with one item that has been changed
replace indexToReplace newValue list = mapWInd (\ x ind -> if ind == indexToReplace then newValue else x ) list

------

type Player = String

getInput :: Player -> Board -> IO Board
getInput icon (Board board) = do
  putStrLn $ mconcat ["Player ", icon, " enter your row (3 to 1 going down)"] 
  inputXString <- getLine
  let x = 3 - (fromMaybe 1 $ readMaybe inputXString)
  putStrLn $ mconcat ["Player ", icon, " enter your column (1 to 3 going accross)"] 
  inputYString <- getLine
  let y = (fromMaybe 1 $ readMaybe inputYString) - 1
  if (x > 3) || (y > 3) 
    then do
      putStrLn "You cant go there"
      getInput icon $ Board board
    else
      if board!!x!!y == " "
        then
          return $ Board $ replace x (replace y icon (board!!x)) board
        else do
          putStrLn "You cant go there"
          getInput icon $ Board board

------

type Case = [[Int]]

checkCase :: Case -> Board -> Bool
checkCase [a, b, c] (Board board) = 
  if (foldr (\a_ b_ -> if (a_ == b_) && (b_ /= " ") then a_ else "#") (board!!(a!!0)!!(a!!1)) items) == "#"
    then
      False
    else
      True
  where
    items = [board!!(a!!0)!!(a!!1), board!!(b!!0)!!(b!!1), board!!(c!!0)!!(c!!1)]


------

hasWon :: Board -> Bool
hasWon board =
  foldr (\a b -> a /= b) False cases
  where
    cases = fmap (\x -> checkCase x board) [
              [[0, 0], [0, 1], [0, 2]]
            , [[1, 0], [1, 1], [1, 2]]
            , [[2, 0], [2, 1], [2, 2]]
            , [[0, 0], [1, 0], [2, 0]]
            , [[0, 1], [1, 1], [2, 1]]
            , [[0, 2], [1, 2], [2, 2]]
            , [[0, 0], [1, 1], [2, 2]]
            , [[0, 2], [1, 1], [2, 0]]
            ]

------

gameLoop :: Player -> Player -> Board -> Player -> IO () 
gameLoop player1 player2 board playerToPlay = 
  if hasWon board
    then do
      drawBoard board
      putStrLn "You Won!"
    else do
      drawBoard board
      board' <- getInput playerToPlay board
      let nextPlayer = if player1 == playerToPlay then player2 else player1
      gameLoop player1 player2 board' nextPlayer


------

main :: IO ()
main = do
  let board = Board [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
  gameLoop "X" "O" board "X"
